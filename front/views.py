from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from models import *
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
import json


def home(request):

	# codigo_usuario = request.COOKIES['csrftoken']
	# print "indentificador de sessao: %s"%request.COOKIES['csrftoken']
	return render_to_response("index.html",{},context_instance=RequestContext(request))


def contato(request):
	# codigo_usuario = request.COOKIES['csrftoken']
	# print "indentificador de sessao: %s"%request.COOKIES['csrftoken']
	return render_to_response("page-contact-us.html",{},context_instance=RequestContext(request))


def sobre(request):
	# codigo_usuario = request.COOKIES['csrftoken']
	# print "indentificador de sessao: %s"%request.COOKIES['csrftoken']
	return render_to_response("page-about-us.html",{},context_instance=RequestContext(request))



def testeImagem(request):
	if request.method == "POST":
		print request
		form = formCadProduto(request.POST, request.FILES)
		if form.is_valid():
			tre = Produtos(
                nome = form.cleaned_data['nome'],
                imagem_apresentacao = form.cleaned_data['imagem_apresentacao'],
                quantidade = form.cleaned_data['quantidade'],
                valor = form.cleaned_data['valor'],
                categoria = form.cleaned_data['categoria'],
                descricao = form.cleaned_data['descricao'],
                )
			tre.save()
			return HttpResponseRedirect('/imagem')
	else:
		form = formCadProduto()
	return render_to_response("galeria.html",{'form': form},context_instance = RequestContext(request))


def send_email(request):
	resposta=''
	subject = request.POST['assunto']
	message = request.POST['mensagem']
	message += ".\n telefone para contato: "+str(request.POST['telefone'])
	message += "\n Nome do cliente: "+str(request.POST['nome'])
	from_email = settings.EMAIL_HOST_USER
	to_list = ['douglasmb11@hotmail.com','andersonsilva452@hotmail.com']
	try:
		send_mail(subject,message,from_email,to_list,fail_silently=True)	
		resposta = True
	except Exception, e:
		resposta = False

	messages.success(request,'mensagem')

	retorno = json.dumps({'resposta':resposta})
	return HttpResponse(retorno,mimetype = 'text/javascript')
	