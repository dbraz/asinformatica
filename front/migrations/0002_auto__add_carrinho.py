# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Carrinho'
        db.create_table(u'front_carrinho', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'front', ['Carrinho'])

        # Adding M2M table for field produto on 'Carrinho'
        m2m_table_name = db.shorten_name(u'front_carrinho_produto')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('carrinho', models.ForeignKey(orm[u'front.carrinho'], null=False)),
            ('produtos', models.ForeignKey(orm[u'produto.produtos'], null=False))
        ))
        db.create_unique(m2m_table_name, ['carrinho_id', 'produtos_id'])


    def backwards(self, orm):
        # Deleting model 'Carrinho'
        db.delete_table(u'front_carrinho')

        # Removing M2M table for field produto on 'Carrinho'
        db.delete_table(db.shorten_name(u'front_carrinho_produto'))


    models = {
        u'front.carrinho': {
            'Meta': {'object_name': 'Carrinho'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'produto': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['produto.Produtos']", 'symmetrical': 'False'})
        },
        u'produto.produtos': {
            'Meta': {'object_name': 'Produtos'},
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'descricao': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem_apresentacao': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'quantidade': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'valor': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['front']