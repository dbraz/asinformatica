function update_image(rsa) {
    if(($('#fileInput').val()!='') && ($('#id_nome').val()!='')){
        console.log('valido');
        $.ajax({
            url : "/update_image/",
            type : "POST",
            dataType: "json",
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            data : {
                'nome':$("#id_nome").val(),
                "img":$("#fileInput").val(),
                'csrfmiddlewaretoken': rsa,
            },
            success : function(json) {
                console.log(json);
                

            },
            error : function(xhr,errmsg,err) {
                console.log(xhr.status + ": " + xhr.responseText);
            }
        });
    }
    else{
        noty({"text": "Dados de pre cadastro incompletos", "layout":"topRight", "type":"error"});
    };
}