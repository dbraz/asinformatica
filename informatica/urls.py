from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'informatica.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^teste/', 'front.views.home', name='home'),
    url(r'^contato/', 'front.views.contato', name='contato'),
    url(r'^send_email/', 'front.views.send_email', name='email'),
    url(r'^sobre/', 'front.views.sobre', name='sobre'),
    url(r'^admin/', include(admin.site.urls)),
)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
